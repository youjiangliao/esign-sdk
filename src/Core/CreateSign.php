<?php
namespace Xinpow\Esign\Core;

use CURLFile;
use Xinpow\Esign\FactoryAbstract;
use Xinpow\Esign\Core\BaseHelper;

class CreateSign extends FactoryAbstract {
    
    use BaseHelper;

    public static function handle($className = __CLASS__) {
        return parent::handle($className);
    }

    /**
     * 摘要签署（平台）
     * 处理流程为从远程读取源文件后在集群内签署并返回流数据.
     * 
     * @param string  $sourceFile 远程的源文件读取地址，需要能被链接访问
     * 
     * @param array   $base      基础信息，具体元素如下
     *                                  {string}   ownerPassword  文档密码，当目标 PDF 设置权限保护的时候必填
     *                                  {string}   signType       签章类型，Single（单页签章）、Multi（多页签章）、Edges（签骑缝章）、Key（关键字签章）
     *                                  {string}   sealId         印章 ID，默认为0，即默认印章
     * 
     * @param array   $sign_pos  签章数据，具体元素：
     *                                      posType         定位类型，0坐标定位，1关键字定位，默认0（若为关键字定位，签章类型（signType）必须指定为关键字定位才生效）
     *                                      posPage         签署页码,
     *                                      posX            签署位置X坐标,原点为左下角
     *                                      posY            签署位置Y坐标,原点为左下角
     *                                      width           签章宽度
     *                                      key             关键字，关键字签章必填
     *                                      addSignTime     是否显示本地签署时间（需要width设置92以上才可以看到时间）
     *                                      qrcodeSign      是否是二维码签署，默认为false，二维码签署不支持骑缝签和多页签
     *                                      cacellingSign   是否是作废签签署，默认为false
     */
    public function signForSelf($sourceFile, $base, $sign_pos = []) {
        $data = [
            'signType' => $base['signType'],
            'sealId'   => $base['sealId'],
            'file'     => new \CURLFile($sourceFile),
        ];
        $data = array_merge($data, $base);
        $data['signPos'] = json_encode(array_merge([
            "posPage" => 1,
            "posType" => 0,
            "posX"    => 100,
            "posY"    => 100,
            "width"   => 100
        ], $sign_pos));
        return $this->doPost($this->_config->war_url . $this->_config->api_map->SELF_STREAM_SIGN, $data, ['Content-Type:multipart/form-data;']);
    }

    /**
     * 摘要签署（用户）
     * 处理流程为从远程读取源文件后在集群内签署并返回流数据
     * 
     * @param array   $base      基础信息，具体元素如下
     *                                  {stream}   file           文件流
     *                                  {string}   ownerPassword  文档密码，当目标 PDF 设置权限保护的时候必填
     *                                  {integer}  accountId      签署账号 ID
     *                                  {string}   sealData       印章图片 base64 数据
     *                                  {string}   mobile         接收验证码的手机
     *                                  {string}   code           短信验证码
     *                                  {string}   signType       签章类型，Single（单页签章）、Multi（多页签章）、Edges（签骑缝章）、Key（关键字签章）
     * 
     * @param array   $sign_pos  签章数据，具体元素：
     *                                      posType         定位类型，0坐标定位，1关键字定位，默认0（若为关键字定位，签章类型（signType）必须指定为关键字定位才生效）
     *                                      posPage         签署页码,
     *                                      posX            签署位置X坐标,原点为左下角
     *                                      posY            签署位置Y坐标,原点为左下角
     *                                      width           签章宽度
     *                                      key             关键字，关键字签章必填
     *                                      addSignTime     是否显示本地签署时间（需要width设置92以上才可以看到时间）
     *                                      qrcodeSign      是否是二维码签署，默认为false，二维码签署不支持骑缝签和多页签
     *                                      cacellingSign   是否是作废签签署，默认为false
     */
    public function signForOther($sourceFile, $base, $sign_pos = []) {
        $data = [
            'signType' => 'Single',
            'file'     => new \CURLFile($sourceFile),
            'signPos'  => [
                'posPage'       => '',
                'posType'       => '',
                'key'           => '',
                'posX'          => '',
                'posY'          => '',
                'width'         => '',
                'qrcodeSign'    => false,
                'cacellingSign' => false,
                'addSignTime'   => false
            ]
        ];
        $data = array_merge($data, $base);
        $data['signPos'] = json_encode(array_merge([
            "posPage" => 1,
            "posType" => 0,
            "posX"    => 100,
            "posY"    => 100,
            "width"   => 100
        ], $sign_pos));
        return $this->doPost($this->_config->war_url . $this->_config->api_map->USER_STREAM_MOBILE_SIGN, $data, ['Content-Type:multipart/form-data;']);
    }

    /**
     * 平台用户PDF摘要签署（文件流）
     * 处理流程为从远程读取源文件后在集群内签署并返回流数据
     * 
     * @param array   $base      基础信息，具体元素如下
     *                                  {stream}   file           文件流
     *                                  {string}   ownerPassword  文档密码，当目标 PDF 设置权限保护的时候必填
     *                                  {integer}  accountId      签署账号 ID
     *                                  {string}   sealData       印章图片 base64 数据
     *                                  {string}   signType       签章类型，Single（单页签章）、Multi（多页签章）、Edges（签骑缝章）、Key（关键字签章）
     * 
     * @param array   $sign_pos  签章数据，具体元素：
     *                                      posType         定位类型，0坐标定位，1关键字定位，默认0（若为关键字定位，签章类型（signType）必须指定为关键字定位才生效）
     *                                      posPage         签署页码,
     *                                      posX            签署位置X坐标,原点为左下角
     *                                      posY            签署位置Y坐标,原点为左下角
     *                                      width           签章宽度
     *                                      key             关键字，关键字签章必填
     *                                      addSignTime     是否显示本地签署时间（需要width设置92以上才可以看到时间）
     *                                      qrcodeSign      是否是二维码签署，默认为false，二维码签署不支持骑缝签和多页签
     *                                      cacellingSign   是否是作废签签署，默认为false
     */
    public function signForUser($sourceFile, $base, $sign_pos = []) {
        $data = [
            'signType' => 'Single',
            'file'     => new \CURLFile($sourceFile),
            'signPos'  => [
                'posPage'       => '',
                'posType'       => '',
                'key'           => '',
                'posX'          => '',
                'posY'          => '',
                'width'         => '',
                'qrcodeSign'    => false,
                'cacellingSign' => false,
                'addSignTime'   => false
            ]
        ];
        $data = array_merge($data, $base);
        $data['signPos'] = json_encode(array_merge([
            "posPage" => 1,
            "posType" => 0,
            "posX"    => 100,
            "posY"    => 100,
            "width"   => 100
        ], $sign_pos));
        return $this->doPost($this->_config->war_url . $this->_config->api_map->USER_FILE_SIGN, $data, ['Content-Type:multipart/form-data;']);
    }
}