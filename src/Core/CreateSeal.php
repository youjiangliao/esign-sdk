<?php
/*
 * 账号印章相关
 * copyright: 有酱料
 * author: Xinpow<xinpow@live.com>
 * lang: zh_CN
 * version: v1.0
 */
namespace Xinpow\Esign\Core;

use Xinpow\Esign\FactoryAbstract;
use Xinpow\Esign\Core\BaseHelper;

class CreateSeal extends FactoryAbstract {
    
    use BaseHelper;

    public static function handle($className = __CLASS__) {
        return parent::handle($className);
    }
    
    /**
     * 创建印章
     * @param string $data 印章数据,包含字段如下:
     *                          * accountId: 必填字段, 签章账号 ID
     *                            color: 印章颜色
     *                            templateType: 印章模版类型, 个人签章可选: SQUARE（正方形），RECTANGLE（长方形），BORDERLESS（无框矩形）
     *                                                       企业签章可选：STAR（标准公章），OVAL（椭圆形印章
     *                            hText: 生成印章中的横向文内容；可设置0-8个字，企业名称超出25个字后，不支持设置横向文；一般是指的XX专用章，如合同专用章、财务专用章等
     *                            qText: 生成印章中的下弦文内容；可设置0-20个字，企业企业名称超出25个字后，不支持设置下弦文；下弦文是指的贵司公章底部一串防伪数字，若没有该参数可不填
     * 
     * @return object
     */
    public function registerSeal($data, $accountType = 'personal') {
        $api = [
            'personal' => $this->_config->war_url . $this->_config->api_map->ADD_PERSON_SEAL,
            'business' => $this->_config->war_url . $this->_config->api_map->ADD_ORG_SEAL
        ];
        $sealData = [
            'color' => 'RED',
            'templateType' => $accountType === 'personal' ? 'RECTANGLE' : 'STAR'
        ];
        $data = array_merge($sealData, $data);
        return $this->doPost($api[$accountType], $data);
    }
}