<?php
namespace Xinpow\Esign\Core;

use Xinpow\Esign\FactoryAbstract;
use Xinpow\Esign\Core\BaseHelper;

class Sms extends FactoryAbstract {
    
    use BaseHelper;

    public static function handle($className = __CLASS__) {
        return parent::handle($className);
    }

    /**
     * 发送签署短信验证码
     * 
     * @param integer $accountId 签署者 ID
     * @param string  $mobile    待接收短信验证码手机
     */
    public function send($accountId, $mobile) {
        $data = [
            'accountId' => $accountId,
            'mobile'    => $mobile
        ];
        return $this->doPost($this->_config->war_url . $this->_config->api_map->SMS_SEND, $data);
    }
}