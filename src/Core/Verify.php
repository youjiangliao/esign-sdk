<?php
/* 数据验证
 *
 * @copyright: 有酱料
 * @author: Xinpow<xinpow@live.com>
 * @lang: zh_CN
 * @version: v1.0
 */
namespace Xinpow\Esign\Core;

use Xinpow\Esign\FactoryAbstract;
use Xinpow\Esign\Core\BaseHelper;

class Verify extends FactoryAbstract {
    
    use BaseHelper;

    public static function handle($className = __CLASS__) {
        return parent::handle($className);
    }
    
    /**
     * 文档验签接口（文件流）
     * 
     * @param file $fileStream 需要验签的文件流数据
     */
    public function doVerify($fileStream) {
        return $this->doPost($this->_config->war_url . $this->_config->api_map->VERIFY_STREAM, ['file' => $fileStream]);
    }
}