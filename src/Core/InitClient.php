<?php
namespace Xinpow\Esign\Core;

use Xinpow\Esign\FactoryAbstract;
use Xinpow\Esign\Core\BaseHelper;

class InitClient extends FactoryAbstract {

    use BaseHelper;

    public static function handle($className = __CLASS__) {
        return parent::handle($className);
    }

    /**
     * 初始化
     * E签宝初始化并注册对接JAVA服务器
     * 
     * @return mixed
     */
    public function doRegister() {
        $data = [
            // 项目服务器配置
            'projectConfig' => [
                'projectId'     => $this->_config->app_id,
                'projectSecret' => $this->_config->app_secret,
                'itsmApiUrl'    => $this->_config->open_api_url->{$this->_config->default_api}
            ]
        ];
        // 算法类型
        if($this->_config->sign_config->enable_algorithm) {
            $data['signConfig'] = [
                'algorithm' => $this->_config->sign_config->algorithm
            ];
        }
        return $this->doPost($this->_config->war_url . $this->_config->api_map->INIT_URL, $data);
    }

}