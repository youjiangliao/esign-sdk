<?php
/*
 * 注册相关签署账号
 * copyright: 有酱料
 * author: Xinpow<xinpow@live.com>
 * lang: zh_CN
 * version: v1.0
 */
namespace Xinpow\Esign\Core;

use Xinpow\Esign\FactoryAbstract;
use Xinpow\Esign\Core\BaseHelper;

class CreateAccount extends FactoryAbstract {
    
    use BaseHelper;

    public static function handle($className = __CLASS__) {
        return parent::handle($className);
    }

    /**
     * 注册账户(企业/个人)
     * 
     * @param string $accountType 更新账户类型，个人账户：personal，企业账户：business
     */
    public function register($data, $accountType = 'personal') {
        $api = [
            'personal' => $this->_config->war_url . $this->_config->api_map->ADD_PERSON_ACCOUNT,
            'business' => $this->_config->war_url . $this->_config->api_map->ADD_ORG_ACCOUNT
        ];
        switch (strtolower($accountType)) {
            case 'personal':
                $rules = [
                    'email'  => 'rule:^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$',
                    'name'   => 'required',
                    'mobile' => 'rule:^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$',
                    'idNo'   => 'rule:(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)'
                ];
                $rules_err = [
                    'email' => [
                        'rule' => '请输入正确的电子邮箱地址'
                    ],
                    'mobile' => [
                        'rule' => '请输入正确的手机号码'
                    ],
                    'idNo' => [
                        'rule' => '请输入正确的身份证号码'
                    ]
                ];
                break;
            default:
                $accountType = 'business';
                $rules = [
                    'name'      => 'required',
                    'regType'   => 'rule:^(NOMAL|MERGE|REGCODE)$',
                    'organCode' => 'required'
                ];
                $rules_err = [
                    'email' => [
                        'rule' => '请输入正确的电子邮箱地址'
                    ],
                    'regType' => [
                        'rule' => '请输入正确的企业注册类型'
                    ],
                    'organCode' => [
                        'rule' => '请输入正确的组织机构代码、工商注册号或者统一社会信用代码'
                    ]
                ];
                break;
        }
        foreach ($rules as $k => $v) {
            if(!in_array($k, array_keys($data)))
                return $this->fail('缺少参数：' . $k);
            $v = explode('||', $v);
            if(in_array('required', $v) && empty($data[$k]))
                return $this->fail('参数 ' . $k . ' 不能为空');

            // 并非严禁判断,将就着用先
            // by Xinpow
            if(strstr(join('||', $v), 'rule:')) {
                preg_match('/rule\:(.+)/is', join('||', $v), $regx);
                if(isset($regx[1]) && $regx[1]) {
                    if(!preg_match('/' . explode('||', $regx[1])[0] . '/i', $data[$k])) {
                        if(isset($rules_err[$k]['rule']))
                            return $this->fail($rules_err[$k]['rule']);
                        else
                            return $this->fail($k . ': Fail rule');
                    }
                }
            }
        }
        return $this->doPost($api[$accountType], $data);
    }

    /**
     * 更新账户(企业/个人)
     * 
     * @param integer $accountId   更新的账户 ID
     * @param array   $data        更新的账户数据
     * @param string  $accountType 更新账户类型，个人账户：personal，企业账户：business
     */
    public function update($accountId, $data = [], $accountType = 'personal') {
        $api = [
            'personal' => $this->_config->war_url . $this->_config->api_map->UPDATE_PERSON_ACCOUNT,
            'business' => $this->_config->war_url . $this->_config->api_map->UPDATE_ORG_ACCOUNT
        ];
        switch (strtolower($accountType)) {
            case 'personal':
                $rules = [
                    'email'  => 'rule:^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$',
                    'mobile' => 'rule:^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$',
                ];
                $rules_err = [
                    'email' => [
                        'rule' => '请输入正确的电子邮箱地址'
                    ],
                    'mobile' => [
                        'rule' => '请输入正确的手机号码'
                    ]
                ];
                break;
            default:
                $accountType = 'business';
                $rules = [
                    'regType'   => 'rule:^(NOMAL|MERGE|REGCODE)$',
                ];
                $rules_err = [
                    'email' => [
                        'rule' => '请输入正确的电子邮箱地址'
                    ],
                    'regType' => [
                        'rule' => '请输入正确的企业注册类型'
                    ],
                    'organCode' => [
                        'rule' => '请输入正确的组织机构代码、工商注册号或者统一社会信用代码'
                    ]
                ];
                break;
        }
        foreach ($rules as $k => $v) {
            $v = explode('||', $v);
            if(in_array('required', $v) && empty($v))
                return $this->fail('参数 ' . $k . ' 不能为空');

            if(isset($data[$k]) && strstr(join('||', $v), 'rule:')) {
                preg_match('/rule:(.+)/is', join('||', $v), $regx);
                if(isset($regx[1]) && $regx[1]) {
                    if(!preg_match('/' . explode('||', $regx[1])[0] . '/i', $data[$k])) {
                        if(isset($rules_err[$k]['rule']))
                            return $this->fail($rules_err[$k]['rule']);
                        else
                            return $this->fail($k . ': Fail rule');
                    }
                }
            }
        }
        $update = [
            'deleteList'     => [],
            'accountId'      => $accountId
        ];
        $update[strtolower($accountType) === 'personal' ? 'updatePerson' : 'updateOrganize'] = $data;
        return $this->doPost($api[$accountType], $update);
    }

    /**
     * 通过证件号获得用户信息
     * 
     * @param string  $idNo 用户证件号
     * @param integer $type 证件类型：11:大陆身份证号、12:香港通行证、13:澳门通行证、14:台湾通行证、15:外籍证件、21:组织机构代码号、22:统一社会信用代码、23:工商注册号
     */
    public function getUser($idNo, $type) {
        return $this->doPost(
            $this->_config->war_url . $this->_config->api_map->GET_ACCOUNT_INFO_BY_IDNO, 
            ['idNo' => $idNo, 'idNoType' => $type]
        );
    }

    /**
     * 注销账户
     * 慎重使用
     */
    public function delete($accountId) {
        return $this->doPost($this->_config->war_url . $this->_config->api_map->DELETE_ACCOUNT, ['accountId' => $accountId]);
    }

}