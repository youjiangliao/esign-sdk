<?php
/*
 * @copyright: 有酱料
 * @author: Xinpow<xinpow@live.com>
 * @lang: zh_CN
 * @version: v1.0
 */
namespace Xinpow\Esign;

class ErrorCode {
    public static $error = [
        'NO_APP_ID'     => '应用 ID (app_id)未配置',
        'NO_APP_SECRET' => '应用密钥未(app_secret)配置'
    ];
}