<?php
namespace Xinpow\Esign;

abstract class FactoryAbstract {

    private function __construct() {}

    private static $_models = array();

    public static function handle($className = __CLASS__) {
        if (isset(self::$_models[$className])) {
            return self::$_models[$className];
        } else {
            return self::$_models[$className] = new $className(null);
        }
    }

}