<?php
/*
 * @copyright: 有酱料
 * @author: Xinpow<xinpow@live.com>
 * @lang: zh_CN
 * @version: v1.0
 */
namespace Xinpow\Esign;

class Config {
    
    protected $_config = [
        'app_id'       => '',
        'app_secret'   => '',
        'default_api'  => 'prod',
        'open_api_url' => [
            'mock' => 'http://121.40.164.61:8080/tgmonitor/rest/app!getAPIInfo2',
            'prod' => 'http://itsm.tsign.cn/tgmonitor/rest/app!getAPIInfo2'
        ],
        // java 服务地址
        'war_url'=>'http://localhost:8080/tech-sdkwrapper/timevale',
        'proxy_config' => [
            'enable_http_proxy' => false,
            'http_proxy_ip'     => '',
            'http_proxy_port'   => '',
            'retry'             => 5,
            'http_type'         => 'HTTPS'
        ],
        'sign_config' => [
            'enable_algorithm' => false,
            'algorithm' => ''
        ],
        // 日志记录存放地址
        'logs_path' => '/logs/esign/',
        'api_map' => [
            'INIT_URL'                    => '/init',                            // 项目初始化

            'SMS_SEND'                    => '/mobile/send3rdCode',              // 签署短信

            'ADD_PERSON_ACCOUNT'          => '/account/addPerson',               // 创建个人账户
            'UPDATE_PERSON_ACCOUNT'       => '/account/updatePerson',            // 更新个人账户
            'ADD_ORG_ACCOUNT'             => '/account/addOrganize',             // 创建企业账户
            'UPDATE_ORG_ACCOUNT'          => '/account/updateOrganize',          // 更新企业账户
            'DELETE_ACCOUNT'              => '/account/delete',                  // 注销账户
            'GET_ACCOUNT_INFO_BY_IDNO'    => '/account/getAccountInfoByIdNo',    // 通过证件号获得账户信息

            'ADD_PERSON_SEAL'             => '/seal/addPersonSeal',              // 创建个人模板印章
            'ADD_ORG_SEAL'                => '/seal/addOrganizeSeal',            // 创建企业模板印章

            'SELF_FILE_SIGN'              => '/sign/selfFileSign',               // 平台自身摘要签署
            'SELF_STREAM_SIGN'            => '/sign/selfStreamSign',             // 平台自身摘要签署（文件流）
            // 'USER_FILE_SIGN'              => '/sign/userFileSign',               // 平台用户PDF摘要签署
            'USER_FILE_SIGN'              => '/sign/userStreamSign',               // 平台用户PDF摘要签署（文件流）
            'USER_STREAM_MOBILE_SIGN'     => '/sign/userStreamMobileSign',       // 平台用户PDF摘要签署（文件流&制定手机短信验证）
            'EVENT_FILE_SIGN'             => '/sign/eventFileSign',              // 事件证书PDF摘要签署
            'EVENT_STREAM_SIGN'           => '/sign/eventStreamSign',            // 事件证书PDF摘要签署（文件流）
            'ADD_EVENT_CERT'              => '/account/addEventCert',            // 创建事件证书
            'CREATE_FROM_TEMPLATE_FILE'   => '/doc/file/createFromTemplate',     // 填充指定PDF模板文件
            'CREATE_FROM_TEMPLATE_STREAM' => '/doc/stream/createFromTemplate',   // 填充指定PDF模板流

            'VERIFY_FILE'                 => '/verify/fileVerify',               // PDF文档验签
            'VERIFY_STREAM'               => '/verify/streamVerify',             // PDF文档验签（文件流）
        ]
    ];

    public function __construct(){
        if(function_exists('config')) {
            $config = config('esign');
            $this->_config = array_merge($this->_config, $config);
        }
    }

    public function __get($name){
        return json_decode(json_encode($this->_config[$name], 320));
    }

}