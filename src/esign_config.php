<?php
return [
    'app_id'       => '',
    'app_secret'   => '',
    'default_api'  => 'prod',
    'open_api_url' => [
        'mock' => 'http://121.40.164.61:8080/tgmonitor/rest/app!getAPIInfo2',
        'prod' => 'http://itsm.tsign.cn/tgmonitor/rest/app!getAPIInfo2'
    ],
    // java 服务地址
    'war_url'      => 'http://localhost:8080/tech-sdkwrapper/timevale',
    'proxy_config' => [
        'enable_http_proxy' => false,
        'http_proxy_ip'     => '',
        'http_proxy_port'   => '',
        'retry'             => 5,
        'http_type'         => 'HTTPS'
    ],
    'sign_config' => [
        'enable_algorithm' => false,
        'algorithm'        => ''
    ],
];