<?php
namespace Xinpow\Esign;

use Illuminate\Support\ServiceProvider;

class EsignServiceProvider extends ServiceProvider {

    public function register() {
        $this->app->singleton('esign', function () {
            return new Factory();
        });
    }

    public function boot() {
        $this->publishes([
            __DIR__ . '/esign_config.php' => config_path('esign.php'),
        ]);
    }

}